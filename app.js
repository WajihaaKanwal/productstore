var myApp = angular.module('myApp', []);

myApp.controller('myController', function ($scope) {


    $scope.products = [
        {name: 'Cake 1', price: 2000},
        {name: 'Cake 2', price: 3000},
        {name: 'Cake 3', price: 4000}
    ];

    $scope.cart = [];
    $scope.totalPrice = 0;

    $scope.addToCart = function (item) {
        item.qty = 1;
        $scope.totalPrice = $scope.totalPrice + item.price;
        $scope.cart.push(item);
    }
});

